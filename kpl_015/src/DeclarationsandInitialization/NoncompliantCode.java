/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DeclarationsandInitialization;

/**
 *
 * @author Riv_wanProduction
 */
public class NoncompliantCode {
    private final int balance; 
  private static final Cycle c = new Cycle();
  // Random Deposit 
  private static final int deposit = (int) (Math.random() * 100);  
 
  public NoncompliantCode() { 
    // Subtract processing fee 
    balance = deposit - 10;  
  }
  
  public static void main(String[] args) { 
    System.out.println("The account balance is: " + c.balance); 
  } 
}
