/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DeclarationsandInitialization;

import static java.util.Collections.list;

/**
 *
 * @author Riv_wanProduction
 */
public class CollectionCompliant {
    List<Integer> list = Arrays.asList(new Integer[] {13, 14, 15}); 
boolean first = true;
  for (final Integer i: list) { 
  Integer item = i; 
  if (first) { 
    first = false; 
    item = new Integer(99); 
  } 
  // Process item 
  System.out.println(" New item: " + item); 
}
System.out.println("Processing list..."); 
for (Integer i: list) { 
  if (first) { 
    first = false; 
    i = new Integer(99); 
  } 
  // Process i 
  System.out.println(" New item: " + i); 
}
  
System.out.println("Modified list?"); 
for (Integer i: list) { 
  System.out.println("List item: " + i); 
} 
}
