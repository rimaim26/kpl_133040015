/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Expressions;

/**
 *
 * @author Riv_wanProduction
 */
public class CompliantBoolean {
    public void exampleEqualOperator(){ 
  Boolean b1 = true; 
  Boolean b2 = true; 
      
  if (b1 == b2) {   // Always equal 
    System.out.println("Always printed"); 
  } 
  
  b1 = Boolean.TRUE; 
  if (b1 == b2) {   // Always equal 
    System.out.println("Always printed"); 
  } 
} 
}
